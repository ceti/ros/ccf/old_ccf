//
// Created by sebastian on 04.05.21.
//

#include "ccf/connection/MqttConnection.h"
#include <ros/ros.h>
#include <memory>

bool MqttConnection::ensureConnection() {

    constexpr int N_ATTEMPT = 30;

    if (!client.is_connected()) {
        ROS_WARN_STREAM("[MqttConnection] Client '" << client.get_client_id() << "' lost connection. Reconnecting.");
        for (int i = 0; i < N_ATTEMPT && !client.is_connected(); ++i) {
            try {
                auto rsp = client.reconnect();
                if (!rsp.is_session_present()) {
                    client.subscribe(topics);
                }
                return true;
            } catch (const mqtt::exception &exception) {
                ROS_ERROR_STREAM("[MqttConnection] Problem during reconnection. Retrying in 1s. Exception: "
                                         << exception.to_string());
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
        }
        ROS_ERROR_STREAM("[MqttConnection] Reconnection failed for client '" << client.get_client_id() << "'.");
        return false;
    }
    return true;
}

bool MqttConnection::send(const std::string &channel, const std::string &message) {

    ROS_INFO_STREAM(
            "[MqttConnection] Sending message to channel " << channel << " with a length of " << message.size() << ".");
    if (ensureConnection()) {
        try {
            int length = message.size();
            void *data = (void *) message.c_str();

            auto pub_msg = mqtt::make_message(channel, data, length);
            pub_msg->set_qos(QOS);
            getClient().publish(pub_msg);
            ROS_INFO_STREAM("[MqttConnection] Message has been sent successfully.");
        }
        catch (const mqtt::exception &exc) {
            ROS_ERROR_STREAM("[MqttConnection] Unable to publish  message. " << exc.what());
            return false;
        }
    } else {
        ROS_ERROR_STREAM("[MqttConnection] Not connected! Unable to listen to messages.");
        return false;
    }
    return true;
}

bool MqttConnection::initializeConnection(std::function<void(std::string, std::string)> callback) {

    ROS_INFO_STREAM("trying to connect to MQTT server.");

    // establish the connection
    try {
        if (!client.is_connected()) {
            ROS_INFO_STREAM_NAMED("MqttUtil", client.get_client_id() << ": Initializing MQTT");
            mqtt::connect_options connOpts;
            connOpts.set_keep_alive_interval(20);
            connOpts.set_connect_timeout(1);
            connOpts.set_clean_session(false);
            auto rsp = client.connect(connOpts);
            // only subscribe if there are topics
            if (!topics.empty()) {
                ROS_INFO_STREAM_NAMED("MqttUtil",
                                      client.get_client_id() << ": Subscribing to " << topics.size() << " topics.");
                auto srsp = client.subscribe(topics);
            }
        } else {
            ROS_WARN_STREAM_NAMED("MqttUtil", client.get_client_id() << ": Client is already connected.");
        }
    } catch (const mqtt::exception &e) {
        ROS_ERROR_STREAM_NAMED("MqttUtil",
                               client.get_client_id() << ": Unable to connect to " << client.get_server_uri() << ".");
        return false;
    }

    // then, set the callback
    messageReceivedCallback = callback;

    // then, start the thread that uses the callback
    mqtt_handler_thread = std::make_unique<std::thread>(&MqttConnection::receiveMessages, this);

    return true;
}

void MqttConnection::receiveMessages() {

    //receive object selections
    ros::Rate loop_rate(200);

    while (ros::ok()) {

        if (ensureConnection()) {

            mqtt::const_message_ptr msg;
            if (getClient().try_consume_message_for(&msg, std::chrono::milliseconds(500))) {
                messageReceivedCallback(msg->get_topic(), msg->get_payload_str());
            }
        } else {
            ROS_ERROR_STREAM("Unable to hold connection to MQTT server.");
        }

        ros::spinOnce();
    }
}

MqttConnection::~MqttConnection() {
    mqtt_handler_thread->join();
    client.disconnect();
}

MqttConnection::MqttConnection(const std::string &mqttAddress, const std::string &client_id) : mqtt_address{
        mqttAddress}, client_id{client_id}, topics(), client(mqttAddress, client_id) {}

void MqttConnection::addTopic(const std::string &topic) { topics.push_back(topic); }

mqtt::client &MqttConnection::getClient() {
    return client;
}
