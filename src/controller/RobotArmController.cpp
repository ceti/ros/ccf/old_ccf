//
// Created by Johannes Mey on 17/01/2021.
//

#include <fstream>
#include <stdexcept>
#include <memory>
#include <google/protobuf/text_format.h>
#include <google/protobuf/util/json_util.h>

#include "ccf/util/NodeUtil.h"
#include "ccf/controller/RobotArmController.h"

using CetiRosToolbox::getParameter;

void RobotArmController::receive(const std::string &channel, const std::string &data) {
    ROS_WARN_STREAM("[RobotArmController] Received message on channel " << channel << ".");
    if (channel == selectionTopic) {
        Selection selection;
        selection.ParseFromString(data);
        selectionAction(selection);
    } else if (channel == initSceneTopic) {
        Scene newScene;
        newScene.ParseFromString(data);
        initScene(newScene);
    } else if (channel == commandTopic) {
        PickPlace pickPlace;
        pickPlace.ParseFromString(data);
        pickPlaceAction(pickPlace);
    } else {
        ROS_WARN_STREAM("[RobotArmController] Received ignored message on channel " << channel << ": " << data);
    }
}

Object *RobotArmController::resolveObject(const std::string &id) {
    for (int i = 0; i < scene->objects_size(); i++) {
        if (scene->objects(i).id() == id) {
            return scene->mutable_objects(i);
        }
    }
    throw std::out_of_range("...  did not find it. ");
}

void RobotArmController::sendScene() {
    if (scene) { // meaning if the (smart) pointer is not a nullptr

        ROS_INFO_STREAM("[RobotArmController] Sending scene with " << scene->objects().size() << " objects.");
        sendToAll(sendSceneTopic, scene->SerializeAsString());
        sceneUpdateAction();

    } else {
        ROS_WARN_STREAM("[RobotArmController] Scene is not initialized yet. not sending it.");
    }
}

bool RobotArmController::removeObject(const Object &object) {
    for (auto it = scene->mutable_objects()->begin(); it != scene->mutable_objects()->end(); ++it) {

        if (it->id() == object.id()) {
            ROS_INFO_STREAM("[RobotArmController] Erasing " << it->id() << " from scene.");
            scene->mutable_objects()->erase(it);
            return true;
        }
    }
    return false;
}

bool RobotArmController::pickAndDrop(Object &robot, Object &object, Object &bin, bool simulateOnly) {
    return removeObject(object);
}

void RobotArmController::initScene(const Scene &newScene) {
    if (scene) { // meaning if the (smart) pointer is not a nullptr
        ROS_INFO_STREAM("[RobotArmController] Resetting scene.");
    } else {
        ROS_INFO_STREAM("[RobotArmController] Initializing scene.");
    }
    scene = std::make_unique<Scene>(newScene);
    ROS_INFO_STREAM("The new scene has " << scene->objects().size() << " objects.");
    sendScene();
}

void RobotArmController::reactToSelectionMessage(std::function<void(Selection)> lambda) {
    selectionAction = std::move(lambda);
}

RobotArmController::RobotArmController(const ros::NodeHandle &nodeHandle, const std::string &cellName)
        : Controller(nodeHandle), scene(nullptr),
          sceneUpdateAction([]() {}),
          pickPlaceAction([](const PickPlace &p) {}),
          selectionAction([](const Selection &s) {}),
          cellName(cellName) {

    selectionTopic = getParameter<std::string>(nodeHandle, "topics/selection","selection");
    initSceneTopic = getParameter(nodeHandle, "topics/initScene",cellName + "/scene/init");
    sendSceneTopic = getParameter(nodeHandle, "topics/sendScene",cellName + "/scene/update");
    commandTopic = getParameter(nodeHandle, "topics/command",cellName + "/command");

}

bool RobotArmController::pickAndPlace(Object &robot, Object &object, Object &location, bool simulateOnly) {
    if (object.orientation().x() == 0 && object.orientation().y() == 0 && location.orientation().x() == 0 &&
        location.orientation().y() == 0) {
        // the objects must not be rotated around z
        // TODO improve float comparison with 0
        object.mutable_pos()->set_x(location.pos().x());
        object.mutable_pos()->set_y(location.pos().y());
        object.mutable_pos()->set_z(location.pos().z() - location.size().height() / 2 + object.size().height() / 2);
        object.mutable_orientation()->set_z(location.orientation().z());
        object.mutable_orientation()->set_w(location.orientation().w());
        return true;
    } else {
        return false;
    }
}


void RobotArmController::reactToPickAndPlaceMessage(std::function<void(PickPlace)> lambda) {
    pickPlaceAction = std::move(lambda);
}

void RobotArmController::reactToSceneUpdateMessage(std::function<void()> lambda) {
    sceneUpdateAction = std::move(lambda);
}

std::shared_ptr<Scene> RobotArmController::getScene() {
    return scene;
}

void RobotArmController::loadScene(const std::string &sceneFile) {
    // read file into string: https://stackoverflow.com/questions/2602013/read-whole-ascii-file-into-c-stdstring
    std::ifstream t(sceneFile);

    if (!t.is_open()) {
        ROS_ERROR_STREAM("[RobotArmController] Unable to open scene config file " << sceneFile);
    }

    std::string str;

    t.seekg(0, std::ios::end); // skip to end of file
    str.reserve(t.tellg()); // reserve memory in the string of the size of the file
    t.seekg(0, std::ios::beg); // go back to the beginning

    // Note the double parentheses! F**k C++! https://en.wikipedia.org/wiki/Most_vexing_parse
    str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

    Scene newScene;
    google::protobuf::util::Status status = google::protobuf::util::JsonStringToMessage(str, &newScene);
    if (!status.ok()) {
        ROS_ERROR_STREAM("[RobotArmController] Unable to parse Json String: " << status.ToString());
    } else {
        ROS_INFO_STREAM("[RobotArmController] Parsed a scene with " << newScene.objects().size() << " objects.");
        initScene(newScene);
    }

    std::string s;
    if (google::protobuf::TextFormat::PrintToString(newScene, &s)) {
        ROS_DEBUG_STREAM("[RobotArmController] Received scene" << std::endl << s);
    } else {
        ROS_WARN_STREAM("[RobotArmController] Scene invalid! partial content: " << newScene.ShortDebugString());
    }
}

const std::string &RobotArmController::getSelectionTopic() const {
    return selectionTopic;
}

void RobotArmController::setSelectionTopic(const std::string &selectionTopic) {
    RobotArmController::selectionTopic = selectionTopic;
}

const std::string &RobotArmController::getInitSceneTopic() const {
    return initSceneTopic;
}

void RobotArmController::setInitSceneTopic(const std::string &initSceneTopic) {
    RobotArmController::initSceneTopic = initSceneTopic;
}

const std::string &RobotArmController::getSendSceneTopic() const {
    return sendSceneTopic;
}

void RobotArmController::setSendSceneTopic(const std::string &sendSceneTopic) {
    RobotArmController::sendSceneTopic = sendSceneTopic;
}

const std::string &RobotArmController::getCommandTopic() const {
    return commandTopic;
}

void RobotArmController::setCommandTopic(const std::string &commandTopic) {
    RobotArmController::commandTopic = commandTopic;
}
