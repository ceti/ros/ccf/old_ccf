/*! \file MoveItRobotArmController.cpp

    \author Sebastian Ebert
    \date 17/01/2021
*/

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // fix boost

#include <ros/ros.h>

#include <ccf/util/scene_constructor_util.h>

#include <ccf/SceneUpdate.h>
#include <ccf/PickDropService.h>
#include <ccf/PickPlaceService.h>
#include <ccf/BinCheck.h>

#include "ccf/controller/MoveItRobotArmController.h"

void MoveItRobotArmController::updateScene(ccf::SceneUpdateRequest &req) {

    Scene newScene;

    for (int i = 0; i < req.ids.size(); i++) {

        auto object = newScene.add_objects();
        auto pos = object->mutable_pos();

        if (req.ids[i] == TABLE_PILLAR_1 || req.ids[i] == TABLE_PILLAR_2 || req.ids[i] == TABLE_PLATE
            || req.ids[i] == TABLE_PILLAR_3 || req.ids[i] == TABLE_PILLAR_4) {
            object->set_type(Object_Type_UNKNOWN);
        } else {
            ccf::BinCheck srv;
            srv.request.id = req.ids[i];

            if (!bin_check_client.call(srv)) {
                ROS_ERROR_STREAM("[MoveItRobotArmController] "
                                 "Unable to call " << bin_check_client.getService() <<
                                                   ". It " << (bin_check_client.exists() ? "exists" : "it does not exist")
                                                   << ". Assuming each object is a BOX.");
            }

            if (srv.response.isBin) {
                object->set_type(Object_Type_BIN);
            } else {
                object->set_type(Object_Type_BOX);
            }
        }

        pos->set_z(req.poses[i].position.z + req.transform.z);

        object->set_id(req.ids[i]);
        pos->set_x(req.poses[i].position.x + req.transform.x);
        pos->set_y(req.poses[i].position.y + req.transform.y);

        object->mutable_orientation()->set_w(req.poses[i].orientation.w);
        object->mutable_orientation()->set_x(req.poses[i].orientation.x);
        object->mutable_orientation()->set_y(req.poses[i].orientation.y);
        object->mutable_orientation()->set_z(req.poses[i].orientation.z);

        auto size = object->mutable_size();
        size->set_length(req.primitives[i].dimensions[0]);
        size->set_width(req.primitives[i].dimensions[1]);
        size->set_height(req.primitives[i].dimensions[2]);

        object->mutable_color()->set_r(req.colors.at(i).r);
        object->mutable_color()->set_g(req.colors.at(i).g);
        object->mutable_color()->set_b(req.colors.at(i).b);
    }

    auto object = newScene.add_objects();
    object->set_type(Object_Type_ARM);
    object->set_id("arm");

    auto pos = object->mutable_pos();
    pos->set_x(req.robot.position.x + req.transform.x);
    pos->set_y(req.robot.position.y + req.transform.y);
    pos->set_z(req.robot.position.z + req.transform.z);

    object->mutable_orientation()->set_w(req.robot.orientation.w);
    object->mutable_orientation()->set_x(req.robot.orientation.x);
    object->mutable_orientation()->set_y(req.robot.orientation.y);
    object->mutable_orientation()->set_z(req.robot.orientation.z);

    auto size = object->mutable_size();
    size->set_width(0);
    size->set_length(0);
    size->set_height(0);

    object->mutable_color()->set_r(1);
    object->mutable_color()->set_g(1);
    object->mutable_color()->set_b(1);

    initScene(newScene);
}

bool MoveItRobotArmController::pickAndDrop(Object &robot, Object &object, Object &bin, bool simulateOnly) {
    // TODO implement simulateOnly = false

    if (simulateOnly) {
        ROS_ERROR_STREAM("[MoveItRobotArmController] pickAndDrop with simulateOnly=false not implemented yet!");
    }

    ccf::PickDropService srv;
    srv.request.object = object.id();
    srv.request.bin = bin.id();
    if (!pick_drop_client.call(srv)) {
        ROS_ERROR_STREAM("[MoveItRobotArmController] "
                         "Unable to call " << pick_drop_client.getService() <<
                                           ". It " << (pick_drop_client.exists() ? "exists" : "it does not exist")
                                           << ".");
        return false;
    }

    // we explicitly do not remove the object right now, so we do not call the super method
    return true;
}

bool MoveItRobotArmController::pickAndPlace(Object &robot, Object &object, Object &location, bool simulateOnly) {

    if (simulateOnly) {
        ROS_ERROR_STREAM("[MoveItRobotArmController] pickAndPlace with simulateOnly=false not implemented yet!");
    }

    ccf::PickPlaceService srv;
    srv.request.object = object.id();
    srv.request.pose.orientation.x = location.orientation().x();
    srv.request.pose.orientation.y = location.orientation().y();
    srv.request.pose.orientation.z = location.orientation().z();
    srv.request.pose.orientation.w = location.orientation().w();

    srv.request.pose.position.x = location.pos().x();
    srv.request.pose.position.y = location.pos().y();
    srv.request.pose.position.z = location.pos().z();
    if (!pick_place_client.call(srv)) {
        ROS_ERROR_STREAM("[MoveItRobotArmController] "
                         "Unable to call " << pick_place_client.getService() <<
                                           ". It " << (pick_place_client.exists() ? "exists" : "it does not exist")
                                           << ".");
        return false;
    }

    // we explicitly do not move the object right now, so we do not call the super method
    return true;
}


MoveItRobotArmController::MoveItRobotArmController(ros::NodeHandle &nodeHandle, const std::string &cellName)
        : RobotArmController(nodeHandle, cellName),
          pick_drop_client(nodeHandle.serviceClient<ccf::PickDropService>("/scene_controller/pick_drop_service")),
          pick_place_client(nodeHandle.serviceClient<ccf::PickPlaceService>("/scene_controller/pick_place_service")) {

    bin_check_client = nodeHandle.serviceClient<ccf::BinCheck>("/scene_controller/check_bin_service");
    get_scene_service = nodeHandle.advertiseService<ccf::SceneUpdateRequest, ccf::SceneUpdateResponse>(
            "updateCcfScene",
            [this](auto &req, auto &res) {
                ROS_INFO_STREAM("[MoveItRobotArmController] Received a scene update from the controller.");
                updateScene(req);
                return true;
            }
    );
}

bool MoveItRobotArmController::reachableObject(const Object &robot, const Object &object) {
    return false;
}

bool MoveItRobotArmController::reachableLocation(const Object &robot, const Object &location, const Object &object) {
    return false;
}

MoveItRobotArmController::~MoveItRobotArmController() {
    get_scene_service.shutdown();
}

