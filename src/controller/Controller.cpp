//
// Created by Johannes Mey on 17/01/2021.
//

#include <fstream>
#include <memory>

#include "ccf/controller/Controller.h"

void Controller::addConnection(std::unique_ptr<Connection> &&connection) {
    connection->initializeConnection([this](auto &&channel, auto &&data) {
        receive(std::forward<decltype(channel)>(channel), std::forward<decltype(data)>(data));
    });
    connections.emplace_back(std::move(connection));
}

Controller::Controller(const ros::NodeHandle &nodeHandle) : nodeHandle(nodeHandle) {
}

void Controller::sendToAll(const std::string &channel, const std::string &message) {
    for (auto &connection : connections) {
        connection->send(channel, message);
    }
}
