//
// Created by Sebastian Ebert on 03.08.20.
//

#include "ros/ros.h"
#include "std_msgs/String.h"

#include <google/protobuf/util/json_util.h>
#include <google/protobuf/message.h>

#include "connector.pb.h"
#include "ccf/SceneUpdate.h"
#include "ccf/BinCheck.h"
#include "ccf/PickDropService.h"
#include "ccf/PickPlaceService.h"
#include "ccf/PickPlace.h"

#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>

#include "ccf/util/scene_constructor_util.h"
#include <grasp_util.h>
#include <franka_gripper_util.h>

#include <geometry_msgs/Pose.h>

#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_listener.h>

#include "ccf/util/scene_collision_object.h"

/**
 * Contains the current scene and additional metadata for the grasping-process.
 */
namespace world_state {

    std::vector<SceneCollisionObject> collision_objects;
    std::vector<std_msgs::ColorRGBA> collision_objects_colors;
    geometry_msgs::Pose exported_robot_pose;
    std::string current_picked_object_id;
    std::string current_bin_id;
    geometry_msgs::Point current_transform;
    double open_amount = 0.078;
    bool is_initialized = false;
    bool is_table_configured = false;
    bool is_simulated_robot = false;
}

/**
 * tf2 buffer to receive current robot state
 */
tf2_ros::Buffer tfBuffer;

void checkAndRemoveSceneCollisionObjectById(const std::string &id) {
    for (int i = 0; i < world_state::collision_objects.size(); i++) {
        if (world_state::collision_objects.at(i).getCollisionObject().id == id) {
            ROS_INFO("[SCENECONTROLLER] Deleting object.");
            world_state::collision_objects.erase(world_state::collision_objects.begin() + i);
            world_state::collision_objects_colors.erase(world_state::collision_objects_colors.begin() + i);
        }
    }
}

std::vector<moveit_msgs::CollisionObject>
getMoveItCollisionObjectList(const std::vector<SceneCollisionObject> &internal_list) {

    std::vector<moveit_msgs::CollisionObject> returned_list;

    for (const SceneCollisionObject &sco: internal_list) {
        returned_list.push_back(sco.getCollisionObject());
    }

    return returned_list;
}

/**
 * Checks if there is a bin concerning to a given id. Important: Bins have ID-prefix "bin".
 * @param bin_id the bins id
 * @return true if object with id is an exisiting bin.
 */
bool isBin(const std::string &bin_id) {

    for (auto &collision_object : world_state::collision_objects) {

        if (collision_object.getCollisionObject().id == bin_id) {
            return (collision_object.getType() == BIN_TYPE);
        }
    }
    return false;
}

/**
 *
 * @return the current number of bins identified by the ID-prefix "bin".
 */
int getNumOfBins() {

    int count = 0;

    for (auto &collision_object : world_state::collision_objects) {

        if (collision_object.getType() == BIN_TYPE) {
            count++;
        }
    }
    return count;
}

/**
 * Constructs an scene-update-object and updates it in the CCF controller.
 * @param n a valid node handle
 * @return true on successful transmission
 */
bool pushSceneUpdate(ros::NodeHandle &n) {

    ROS_INFO("[SCENECONTROLLER] Creating dump of current scene.");

    ccf::SceneUpdate::Request req;

    std::vector<geometry_msgs::Pose> poses;
    std::vector<shape_msgs::SolidPrimitive> primitives;
    std::vector<std_msgs::ColorRGBA> colors{world_state::collision_objects_colors};
    std::vector<std::string> ids;

    for (auto &collision_object : world_state::collision_objects) {

        if (collision_object.getType() != BUFFER_TYPE) {

            moveit_msgs::CollisionObject col_obj = collision_object.getCollisionObject();
            geometry_msgs::Pose pose_obj;
            pose_obj.orientation.w = col_obj.primitive_poses[0].orientation.w;
            pose_obj.orientation.x = col_obj.primitive_poses[0].orientation.x;
            pose_obj.orientation.y = col_obj.primitive_poses[0].orientation.y;
            pose_obj.orientation.z = col_obj.primitive_poses[0].orientation.z;

            pose_obj.position.x = col_obj.primitive_poses[0].position.x;
            pose_obj.position.y = col_obj.primitive_poses[0].position.y;
            pose_obj.position.z = col_obj.primitive_poses[0].position.z;

            shape_msgs::SolidPrimitive primitive;
            primitive.dimensions.push_back(col_obj.primitives[0].dimensions[0]);
            primitive.dimensions.push_back(col_obj.primitives[0].dimensions[1]);
            primitive.dimensions.push_back(col_obj.primitives[0].dimensions[2]);

            primitive.type = primitive.BOX;

            poses.push_back(pose_obj);
            primitives.push_back(primitive);
            ids.push_back(col_obj.id);
        }
    }

    req.ids = ids;
    req.poses = poses;
    req.colors = colors;
    req.primitives = primitives;
    req.robot = world_state::exported_robot_pose;
    req.transform.x = world_state::current_transform.x;
    req.transform.y = world_state::current_transform.y;
    req.transform.z = world_state::current_transform.z;


    ros::ServiceClient client = n.serviceClient<ccf::SceneUpdate>("/connector_node_ros_ccf/updateCcfScene");
    ccf::SceneUpdate srv;
    srv.request = req;
    return client.call(srv);
}

/**
 * Creates a table and adds it to the scene.
 * @param planning_scene_interface a valid PlanningSceneInterface instance
 */
bool configTable(Scene &table_scene, moveit::planning_interface::PlanningSceneInterface &planning_scene_interface) {

    ROS_INFO("[SCENECONTROLLER] Adding new table config");

    if (!SceneConstructorUtil::constructTable(world_state::collision_objects,
                                              world_state::collision_objects_colors, table_scene)) {
        return false;
    }

    planning_scene_interface.applyCollisionObjects(getMoveItCollisionObjectList(world_state::collision_objects));
    world_state::is_table_configured = true;
    return true;
}

/**
 * Configures the bins and triggers an update in the CCF controller.
 * @param planning_scene_interface a valid PlanningSceneInterface instance
 * @param n a valid nodehandle
 */
bool configBins(const Scene &scene, moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
                ros::NodeHandle &n) {

    ROS_INFO("[SCENECONTROLLER] Received new bin config");

    std::vector<std::string> binsToDeleteForUpdate;

    for (const auto &obj: scene.objects()) {
        if (obj.type() == Object::BIN) {
            checkAndRemoveSceneCollisionObjectById(obj.id());
            binsToDeleteForUpdate.push_back(obj.id());
        }
    }

    planning_scene_interface.removeCollisionObjects(binsToDeleteForUpdate);

    if (!SceneConstructorUtil::constructBins(world_state::collision_objects,
                                             world_state::collision_objects_colors, scene)) {
        return false;
    }

    planning_scene_interface.applyCollisionObjects(getMoveItCollisionObjectList(world_state::collision_objects));
    return true;
}

bool configObjects(const Scene &scene, moveit::planning_interface::PlanningSceneInterface &planning_scene_interface) {

    std::vector<std::string> objectsToDeleteForUpdate;

    for (const auto &obj: scene.objects()) {
        if (obj.type() == Object::BOX) {
            checkAndRemoveSceneCollisionObjectById(obj.id());
            objectsToDeleteForUpdate.push_back(obj.id());
        }
    }

    planning_scene_interface.removeCollisionObjects(objectsToDeleteForUpdate);

    if (!SceneConstructorUtil::constructObjects(world_state::collision_objects,
                                                world_state::collision_objects_colors, scene)) {
        return false;
    }

    planning_scene_interface.applyCollisionObjects(getMoveItCollisionObjectList(world_state::collision_objects));
    return true;
}

Scene applyInputTransformation(const Scene &scene) {

    bool found_arm = false;

    Scene newScene;

    for (auto const &object : scene.objects()) {
        if (object.type() == Object::ARM) {
            world_state::current_transform.x = object.pos().x();
            world_state::current_transform.y = object.pos().y();
            // "normal to moveit"
            world_state::current_transform.z = object.pos().z() - (object.size().height() / 2);
            found_arm = true;
            break;
        }
    }

    if (found_arm) {
        for (auto object : scene.objects()) {
            if (object.type() != Object::ARM) {
                auto new_object = newScene.add_objects();
                auto new_pos = new_object->mutable_pos();

                auto pos = object.pos();

                new_pos->set_x(pos.x() - world_state::current_transform.x);
                new_pos->set_y(pos.y() - world_state::current_transform.y);
                // "normal to moveit"
                new_pos->set_z(pos.z() - world_state::current_transform.z);

                new_object->set_id(object.id());
                new_object->set_type(object.type());

                new_object->mutable_orientation()->set_w(object.orientation().w());
                new_object->mutable_orientation()->set_x(object.orientation().x());
                new_object->mutable_orientation()->set_y(object.orientation().y());
                new_object->mutable_orientation()->set_z(object.orientation().z());

                auto size = new_object->mutable_size();
                size->set_length(object.size().length());
                size->set_width(object.size().width());
                size->set_height(object.size().height());

                new_object->mutable_color()->set_r(object.color().r());
                new_object->mutable_color()->set_g(object.color().g());
                new_object->mutable_color()->set_b(object.color().b());
            }
        }
    }
    return newScene;
}


/**
 * Configures and updates the objects to pick/place and triggers an update in the CCF controller.
 * @param msg a message containing the scene
 * @param planning_scene_interface a valid PlanningSceneInterface instance
 * @param n a valid nodehandle
 */
void configSceneCallback(const std_msgs::String::ConstPtr &msg,
                         moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
                         ros::NodeHandle &n) {

    ROS_INFO("[SCENECONTROLLER] Received new config");

    Scene scene;
    google::protobuf::util::JsonStringToMessage(msg->data, &scene);
    scene = applyInputTransformation(scene);

    // STEP 1: configure the table
    if (!world_state::is_table_configured) {
        if (!configTable(scene, planning_scene_interface)) {
            ROS_ERROR_STREAM("[SCENECONTROLLER] Config invalid. Reason: Table not complete.");
            return;
        }
        ROS_INFO("[SCENECONTROLLER] Table successfully configured.");
    }

    // STEP 2: configure the bins
    if (!configBins(scene, planning_scene_interface, n)) {
        ROS_ERROR_STREAM("[SCENECONTROLLER] Config invalid. Reason: No bin provided.");
        return;
    }

    ROS_INFO("[SCENECONTROLLER] Bins successfully configured.");

    // STEP 3: configure the objects
    if (!configObjects(scene, planning_scene_interface)) {
        ROS_ERROR_STREAM("[SCENECONTROLLER] Config invalid. Reason: No bin provided.");
        return;
    }

    ROS_INFO("[SCENECONTROLLER] Objects successfully configured.");

    // STEP 4: push an update to the scene transmitting part
    if (world_state::collision_objects.size() > (5 + getNumOfBins())) {
        pushSceneUpdate(n);
    }
}

/**
 * Picks a specified object
 * @param objectId ID of the object to pick
 * @param planning_scene_interface a valid PlanningSceneInterface instance
 * @param group a initialized MoveGroupInterface instance
 * @param n a valid nodehandle
 */
bool pick(const std::string &objectId,
          moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
          moveit::planning_interface::MoveGroupInterface &group, ros::NodeHandle &n) {
    GraspUtil grasp_util;

    geometry_msgs::Vector3 dimensions;
    moveit_msgs::CollisionObject object_to_pick;

    ROS_INFO("[SCENECONTROLLER] Starting pick planning.");

    std::vector<std::string> touch_links;
    touch_links.push_back("panda_link1");
    touch_links.push_back("panda_link0");

    // just attach the objects before the initial pick operation
    std::vector<std::string> knownObjects = planning_scene_interface.getKnownObjectNames();

    if (std::find(knownObjects.begin(), knownObjects.end(), TABLE_PLATE) != knownObjects.end()) {
        group.attachObject(TABLE_PLATE, "panda_link0", touch_links);
    }

    if (world_state::current_picked_object_id.empty()) {

        std::map<std::string, moveit_msgs::CollisionObject> o_map = planning_scene_interface.getObjects();

        for (auto const &x : o_map) {

            if (x.first == objectId) {
                ROS_INFO("[SCENECONTROLLER] Found object to pick in planning scene");
                object_to_pick = x.second;
            }
        }

        dimensions.x = object_to_pick.primitives[0].dimensions[0];
        dimensions.y = object_to_pick.primitives[0].dimensions[1];
        dimensions.z = object_to_pick.primitives[0].dimensions[2];

        geometry_msgs::Transform gripper_transform = tfBuffer.lookupTransform("panda_hand", "panda_link8",
                                                                              ros::Time(0)).transform;

        ROS_INFO("[SCENECONTROLLER] Computing pre pick pose.");
        geometry_msgs::Pose pick_pose = grasp_util.getPickFromAbovePose(object_to_pick.primitive_poses[0], dimensions,
                                                                        gripper_transform.rotation);

        bool success = grasp_util.pickFromAbove(group, pick_pose, dimensions, world_state::open_amount,
                                                TABLE_PLATE, objectId);
        if (success) {
            world_state::current_picked_object_id = objectId;
            return true;
        }

        return false;
    }

    ROS_WARN("[SCENECONTROLLER] Pick planning failed: An object is currently grasped.");
    return false;
}

geometry_msgs::Pose getBaseLinkPose() {

    geometry_msgs::Pose pose;
    geometry_msgs::TransformStamped transformStamped = tfBuffer.lookupTransform("world", "panda_link0",
                                                                                ros::Time(0));

    pose.position.x = transformStamped.transform.translation.x;
    pose.position.y = transformStamped.transform.translation.y;
    pose.position.z = transformStamped.transform.translation.z;

    pose.orientation.x = transformStamped.transform.rotation.x;
    pose.orientation.y = transformStamped.transform.rotation.y;
    pose.orientation.z = transformStamped.transform.rotation.z;
    pose.orientation.w = transformStamped.transform.rotation.w;

    return pose;
}

bool place(geometry_msgs::Pose target_pose,
           moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
           moveit::planning_interface::MoveGroupInterface &group, ros::NodeHandle &n) {

    ROS_INFO("[SCENECONTROLLER] Starting place planning.");

    if (!world_state::current_picked_object_id.empty()) {

        GraspUtil grasp_util;

        geometry_msgs::Pose object_to_pick_drop;
        moveit_msgs::CollisionObject picked_collision_object;
        moveit_msgs::CollisionObject support_collision_object;

        for (const SceneCollisionObject &sco : world_state::collision_objects) {
            if (sco.getCollisionObject().id == world_state::current_picked_object_id) {
                picked_collision_object = sco.getCollisionObject();
            }
        }

        ROS_INFO("[SCENECONTROLLER] Placing object.");

        support_collision_object.header.frame_id = "panda_link0";
        support_collision_object.id = "place_support-" + std::to_string(rand());

        support_collision_object.primitives.resize(1);
        support_collision_object.primitives[0].type = support_collision_object.primitives[0].BOX;
        support_collision_object.primitives[0].dimensions.resize(3);
        support_collision_object.primitives[0].dimensions = picked_collision_object.primitives[0].dimensions;
        support_collision_object.primitives[0].dimensions[2] =
                std::abs(target_pose.position.z) - (picked_collision_object.primitives[0].dimensions[2] / 2);

        support_collision_object.primitive_poses.resize(1);
        support_collision_object.primitive_poses[0].position.x = target_pose.position.x;
        support_collision_object.primitive_poses[0].position.y = target_pose.position.y;
        support_collision_object.primitive_poses[0].position.z =
                support_collision_object.primitives[0].dimensions[2] / 2;

        support_collision_object.operation = support_collision_object.ADD;
        planning_scene_interface.applyCollisionObject(support_collision_object);

        bool success = grasp_util.placeFromAbove(group, target_pose, world_state::open_amount,
                                                 support_collision_object.id,
                                                 world_state::current_picked_object_id);

        std::vector<std::string> col_id_vec;
        col_id_vec.push_back(support_collision_object.id);
        planning_scene_interface.removeCollisionObjects(col_id_vec);
        pushSceneUpdate(n);

        if (success) {
            world_state::current_picked_object_id = "";
        }
        return success;
    }
    return false;
}

/**
 * Places the currently selected object in the bin.
 * @param msg -
 * @param planning_scene_interface a valid PlanningSceneInterface instance
 * @param group a initialized MoveGroupInterface instance
 * @param n a valid nodehandle
 */
bool drop(const std::string &bin_id,
          moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
          moveit::planning_interface::MoveGroupInterface &group, ros::NodeHandle &n) {

    if (!isBin(bin_id)) {
        ROS_ERROR("[SCENECONTROLLER] Tried to place in not existing bin.");
    }

    ROS_INFO("[SCENECONTROLLER] Starting drop planning.");

    if (!world_state::current_picked_object_id.empty()) {

        GraspUtil grasp_util;

        geometry_msgs::Pose object_to_pick_drop;
        moveit_msgs::CollisionObject bin_collision_object;
        moveit_msgs::CollisionObject picked_collision_object;

        std::map<std::string, moveit_msgs::CollisionObject> o_map = planning_scene_interface.getObjects();

        for (auto const &x : o_map) {
            if (x.first == bin_id) {
                bin_collision_object = x.second;
            }
        }

        for (const SceneCollisionObject &sco : world_state::collision_objects) {
            if (sco.getCollisionObject().id == world_state::current_picked_object_id) {
                picked_collision_object = sco.getCollisionObject();
            }
        }

        object_to_pick_drop.position.x = bin_collision_object.primitive_poses[0].position.x;
        object_to_pick_drop.position.y = bin_collision_object.primitive_poses[0].position.y;
        object_to_pick_drop.position.z = (picked_collision_object.primitives[0].dimensions[2] / 2) +
                                         bin_collision_object.primitives[0].dimensions[2];

        object_to_pick_drop.orientation.w = bin_collision_object.primitive_poses[0].orientation.w;
        object_to_pick_drop.orientation.x = bin_collision_object.primitive_poses[0].orientation.x;
        object_to_pick_drop.orientation.y = bin_collision_object.primitive_poses[0].orientation.y;
        object_to_pick_drop.orientation.z = bin_collision_object.primitive_poses[0].orientation.z;

        ROS_INFO("[SCENECONTROLLER] dropping object.");
        if (!grasp_util.placeFromAbove(group, object_to_pick_drop, world_state::open_amount, bin_id,
                                       world_state::current_picked_object_id)) {
            return false;
        }

        ROS_INFO("[SCENECONTROLLER] Removing placed object from scene.");
        std::vector<std::string> object_remove_vector{world_state::current_picked_object_id};
        planning_scene_interface.removeCollisionObjects(object_remove_vector);

        for (int i = 0; i < world_state::collision_objects.size(); i++) {
            if (world_state::collision_objects.at(i).getCollisionObject().id == world_state::current_picked_object_id) {
                world_state::collision_objects.erase(world_state::collision_objects.begin() + i);
                world_state::collision_objects_colors.erase(world_state::collision_objects_colors.begin() + i);
                break;
            }
        }
        pushSceneUpdate(n);
        world_state::current_picked_object_id = "";
        return true;
    }

    ROS_WARN("[SCENECONTROLLER] Place planning failed: No object currently grasped.");
    return false;
}

/**
 * Callback for pick-action
 * @param msg a message containing the ID of the object to pick
 * @param planning_scene_interface a valid PlanningSceneInterface instance
 * @param group a initialized MoveGroupInterface instance
 * @param n a valid nodehandle
 */
void pickObjectCallback(const std_msgs::String::ConstPtr &msg,
                        moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
                        moveit::planning_interface::MoveGroupInterface &group, ros::NodeHandle &n) {
    pick(msg->data, planning_scene_interface, group, n);
}

/**
 * Callback for place-action
 * @param msg ID of bin where to place
 * @param planning_scene_interface a valid PlanningSceneInterface instance
 * @param group a initialized MoveGroupInterface instance
 * @param n a valid nodehandle
 */
void dropObjectCallback(const std_msgs::String::ConstPtr &msg,
                        moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
                        moveit::planning_interface::MoveGroupInterface &group, ros::NodeHandle &n) {
    drop(msg->data, planning_scene_interface, group, n);
}

/**
 * Callback method for picking an object and placing it at a "arbitrary" location.
 * @param req containing the objects id and the target pose
 * @param res contains a success flag
 * @param n a valid node handle
 * @param planning_scene_interface a initialized planning_scene_interface
 * @param group a move group to plan with
 * @return true on success
 */
bool pickAndPlaceObjectCallback(ccf::PickPlaceService::Request &req,
                                ccf::PickPlaceService::Response &res, ros::NodeHandle &n,
                                moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
                                moveit::planning_interface::MoveGroupInterface &group) {

    bool is_valid_id = false;
    std::string id = req.object;

    for (const SceneCollisionObject &collisionObject : world_state::collision_objects) {
        if (collisionObject.getCollisionObject().id == id) {
            is_valid_id = true;
            break;
        }
    }

    if (is_valid_id) {

        if (!world_state::is_simulated_robot) {
            FrankaGripperUtil frankaUtil;
            frankaUtil.resetGripperForNextAction();
            ROS_INFO_STREAM("[SCENECONTROLLER] Resetting franka gripper for next action.");
        }

        ROS_INFO_STREAM("[SCENECONTROLLER] Picking object.");
        if (!pick(id, planning_scene_interface, group, n)) {
            res.success = false;
            return false;
        }

        system("rosrun dynamic_reconfigure dynparam set /move_group/pick_place cartesian_motion_step_size 0.005");

        ROS_INFO_STREAM("[SCENECONTROLLER] Placing object.");
        if (!place(req.pose, planning_scene_interface, group, n)) {
            res.success = false;
            return false;
        }

        res.success = true;
        return true;
    }

    ROS_ERROR_STREAM("[SCENECONTROLLER] Error while placing object with id " << id << ". Reason: ID NOT FOUND.");
    res.success = false;
    return false;
}

/**
 * Callback method to check if a object with the given ID is a valid bin.
 * @param req request containing the object id
 * @param res contains true if object with id is a valid bin
 * @return true
 */
bool checkBin(ccf::BinCheck::Request &req,
              ccf::BinCheck::Response &res) {
    ROS_DEBUG("[SCENECONTROLLER] Received binCheck-request.");

    bool is_bin = isBin(req.id);
    res.isBin = is_bin;

    return true;
}

/**
 * Pushs manually the current scene
 * @param msg any text message
 * @param n a valid nodehandle
 */
void manualScenePush(const std_msgs::String::ConstPtr &msg, ros::NodeHandle &n) {

    ROS_INFO("[SCENECONTROLLER] Pushing manually a scene update.");
    pushSceneUpdate(n);
}

/**
 * Callback for combined pick-and-drop-action
 * @param req a message containing the ID of the object to pick/drop
 * @param planning_scene_interface a valid PlanningSceneInterface instance
 * @param group a initialized MoveGroupInterface instance
 * @param n true on success
 */
bool pickAndDropObjectCallback(ccf::PickDropService::Request &req,
                               ccf::PickDropService::Response &res, ros::NodeHandle &n,
                               moveit::planning_interface::PlanningSceneInterface &planning_scene_interface,
                               moveit::planning_interface::MoveGroupInterface &group) {

    world_state::current_bin_id = req.bin;

    bool is_valid_id = false;
    std::string id = req.object;

    for (const SceneCollisionObject &collisionObject : world_state::collision_objects) {
        if (collisionObject.getCollisionObject().id == id) {
            is_valid_id = true;
            break;
        }
    }

    if (is_valid_id) {

        if (!world_state::is_simulated_robot) {
            FrankaGripperUtil frankaUtil;
            frankaUtil.resetGripperForNextAction();
            ROS_INFO_STREAM("[SCENECONTROLLER] Resetting franka gripper for next action.");
        }

        ROS_INFO_STREAM("[SCENECONTROLLER] Picking object.");
        if (!pick(id, planning_scene_interface, group, n)) {
            res.success = false;
            return false;
        }

        system("rosrun dynamic_reconfigure dynparam set /move_group/pick_place cartesian_motion_step_size 0.003");

        ROS_INFO_STREAM("[SCENECONTROLLER] Dropping object.");
        bool success = drop(world_state::current_bin_id, planning_scene_interface, group, n);
        res.success = success;
        return success;
    }

    res.success = false;
    return false;
}

int main(int argc, char **argv) {
    ros::init(argc, argv, "scene_controller");
    ros::NodeHandle n("scene_controller");
    ros::AsyncSpinner spinner(7);
    spinner.start();

    tf2_ros::TransformListener tfListener(tfBuffer);

    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    moveit::planning_interface::MoveGroupInterface group("panda_arm");

    if (!world_state::is_initialized) {
        // initial configs
        world_state::exported_robot_pose = getBaseLinkPose();
        ROS_INFO_STREAM("[SCENECONTROLLER] Exporting robot base_link pose.");

        ros::param::get("is_simulated_robot", world_state::is_simulated_robot);

        if (!world_state::is_simulated_robot) {

            ROS_ERROR_STREAM("[SCENECONTROLLER] Using setup for real robot.");


            if (!ros::param::has("max_grasp_approach_velocity") ||
                !ros::param::has("max_grasp_approach_acceleration") ||
                !ros::param::has("max_grasp_transition_velocity") ||
                !ros::param::has("max_grasp_transition_acceleration")) {

                ROS_WARN_STREAM(
                        "[SCENECONTROLLER] Velocities and accelerations are not completely configured. -> Fallback to default value. ");

                ros::param::set("max_grasp_approach_velocity", 0.05);
                ros::param::set("max_grasp_approach_acceleration", 0.05);

                ros::param::set("max_grasp_transition_velocity", 0.05);
                ros::param::set("max_grasp_transition_acceleration", 0.05);
            }

            if (!ros::param::has("tud_grasp_force")) {
                ROS_WARN_STREAM(
                        "[SCENECONTROLLER] Grasp force is not configured. -> Fallback to default value. ");
                ros::param::set("tud_grasp_force", 9.5);
            } else {
                double tud_grasp_fource = 0;
                ros::param::get("tud_grasp_force", tud_grasp_fource);
                ROS_INFO_STREAM("[SCENECONTROLLER] Applied tud_grasp_force " << tud_grasp_fource);
            }
        }

        world_state::is_initialized = true;
        ROS_INFO_STREAM("[SCENECONTROLLER] Initial ccf setup finished.");
    }

    ros::Subscriber sub_co = n.subscribe<std_msgs::String>("/scene_config", 1000,
                                                           boost::bind(&configSceneCallback, _1,
                                                                       boost::ref(planning_scene_interface),
                                                                       boost::ref(n)));

    ros::Subscriber sub_pick = n.subscribe<std_msgs::String>("/robot/pick", 1000,
                                                             boost::bind(&pickObjectCallback, _1,
                                                                         boost::ref(planning_scene_interface),
                                                                         boost::ref(group),
                                                                         boost::ref(n)));

    ros::Subscriber sub_drop = n.subscribe<std_msgs::String>("/robot/drop", 1000,
                                                             boost::bind(&dropObjectCallback, _1,
                                                                         boost::ref(planning_scene_interface),
                                                                         boost::ref(group),
                                                                         boost::ref(n)));

    ros::Subscriber sub_scene_push = n.subscribe<std_msgs::String>("/manual_scene_push", 1000,
                                                                   boost::bind(&manualScenePush, _1,
                                                                               boost::ref(n)));

    ros::ServiceServer service = n.advertiseService("check_bin_service", checkBin);

    ros::ServiceServer pick_drop_service = n.advertiseService<ccf::PickDropService::Request, ccf::PickDropService::Response>(
            "pick_drop_service",
            boost::bind(&pickAndDropObjectCallback, _1, _2, boost::ref(n), boost::ref(planning_scene_interface),
                        boost::ref(group)));

    ros::ServiceServer pick_place_service = n.advertiseService<ccf::PickPlaceService::Request, ccf::PickPlaceService::Response>(
            "pick_place_service",
            boost::bind(&pickAndPlaceObjectCallback, _1, _2, boost::ref(n), boost::ref(planning_scene_interface),
                        boost::ref(group)));

    ros::waitForShutdown();

    return 0;
}
