//
// Created by sebastian on 14.01.21.
//

#include "ccf/util/scene_collision_object.h"

const moveit_msgs::CollisionObject &SceneCollisionObject::getCollisionObject() const {
    return SceneCollisionObject::collisionObject;
}

void SceneCollisionObject::setCollisionObject(const moveit_msgs::CollisionObject &collisionObject) {
    SceneCollisionObject::collisionObject = collisionObject;
}

const std::string &SceneCollisionObject::getType() const {
    return SceneCollisionObject::type;
}

void SceneCollisionObject::setType(const std::string &type) {
    SceneCollisionObject::type = type;
}

SceneCollisionObject::SceneCollisionObject() = default;