//
// Created by Sebastian Ebert on 03.08.20.
//
#include <ros/ros.h>
#include "ccf/util/scene_constructor_util.h"

/**
 * Build a collision object from a part (originated by configuration message of table)
 * @param part
 * @param id
 * @param collision_objects_colors
 * @return
 */
moveit_msgs::CollisionObject buildTablePartFromMsg(const Object &part, std::string id,
                                                   std::vector<std_msgs::ColorRGBA> &collision_objects_colors) {

    moveit_msgs::CollisionObject obj;
    std_msgs::ColorRGBA obj_color;

    obj.header.frame_id = "panda_link0";

    obj.id = id;

    obj.primitives.resize(1);
    obj.primitives[0].type = obj.primitives[0].BOX;
    obj.primitives[0].dimensions.resize(3);
    obj.primitives[0].dimensions[0] = part.size().length();
    obj.primitives[0].dimensions[1] = part.size().width();
    obj.primitives[0].dimensions[2] = part.size().height();

    obj.primitive_poses.resize(1);
    obj.primitive_poses[0].position.x = part.pos().x();
    obj.primitive_poses[0].position.y = part.pos().y();
    obj.primitive_poses[0].position.z = part.pos().z();

    obj.primitive_poses[0].orientation.w = part.orientation().w();
    obj.primitive_poses[0].orientation.x = part.orientation().x();
    obj.primitive_poses[0].orientation.y = part.orientation().y();
    obj.primitive_poses[0].orientation.z = part.orientation().z();

    obj.operation = obj.ADD;

    obj_color.r = part.color().r();
    obj_color.g = part.color().g();
    obj_color.b = part.color().b();
    obj_color.a = 1;

    collision_objects_colors.push_back(obj_color);

    return obj;
}

bool SceneConstructorUtil::constructTable(std::vector<SceneCollisionObject> &collision_objects,
                                          std::vector<std_msgs::ColorRGBA> &collision_objects_colors, Scene &table) {

    int partCount = 0;

    for (auto part: table.objects()) {
        if (part.type() == Object::UNKNOWN) {
            if (part.id() == TABLE_PILLAR_1 || part.id() == TABLE_PILLAR_2 ||
                part.id() == TABLE_PILLAR_3 || part.id() == TABLE_PILLAR_4 ||
                part.id() == TABLE_PLATE) {
                partCount++;

                SceneCollisionObject pillar_sco;
                pillar_sco.setType(PILLAR_TYPE);
                pillar_sco.setCollisionObject(buildTablePartFromMsg(part, part.id(), collision_objects_colors));
                collision_objects.push_back(pillar_sco);

                ROS_INFO_STREAM("[SCENE-UTIL] Found table part " << part.id() << ".");
            }
        }
    }

    if (partCount < 5) {
        ROS_WARN(
                "[SCENECONTROLLER] Invalid table config, please insert 4 pillars and on place. "
                "Required IDs: tablePillar1, tablePillar2, tablePillar3, tablePillar4, table");
        return false;
    }
    return true;
}

bool SceneConstructorUtil::constructBins(std::vector<SceneCollisionObject> &collision_objects,
                                         std::vector<std_msgs::ColorRGBA> &collision_objects_colors,
                                         const Scene &scene) {

    for (const auto &obj : scene.objects()) {
        if (obj.type() == Object::BIN) {

            ROS_INFO("[SCENE-UTIL] Creating new bin");

            moveit_msgs::CollisionObject bin;
            std_msgs::ColorRGBA bin_color;

            bin.header.frame_id = "panda_link0";
            bin.id = obj.id();

            bin.primitives.resize(1);
            bin.primitives[0].type = bin.primitives[0].BOX;
            bin.primitives[0].dimensions.resize(3);
            bin.primitives[0].dimensions[0] = obj.size().length();
            bin.primitives[0].dimensions[1] = obj.size().width();
            bin.primitives[0].dimensions[2] = obj.size().height();

            bin.primitive_poses.resize(1);
            bin.primitive_poses[0].position.x = obj.pos().x();
            bin.primitive_poses[0].position.y = obj.pos().y();
            bin.primitive_poses[0].position.z = obj.pos().z();

            bin.primitive_poses[0].orientation.w = obj.orientation().w();
            bin.primitive_poses[0].orientation.x = obj.orientation().x();
            bin.primitive_poses[0].orientation.y = obj.orientation().y();
            bin.primitive_poses[0].orientation.z = obj.orientation().z();

            bin.operation = bin.ADD;

            bin_color.r = obj.color().r();
            bin_color.g = obj.color().g();
            bin_color.b = obj.color().b();
            bin_color.a = 1;

            SceneCollisionObject bin_sco;
            bin_sco.setType(BIN_TYPE);
            bin_sco.setCollisionObject(bin);
            collision_objects.push_back(bin_sco);
            collision_objects_colors.push_back(bin_color);
        }
    }

    if (scene.objects().size() == 0) {
        ROS_INFO("[SCENE-UTIL] NO NEW BIN PROVIDED!");
        return false;
    }

    return true;
}

/**
 * Check if object in a scene have valid dimensions and a ids.
 * @param scene scene with collision objects
 * @return true if valid
 */
bool validateObjects(const Scene &scene) {
    for (const auto &obj : scene.objects()) {
        if (obj.size().length() == 0 || obj.size().width() == 0 || obj.size().height() == 0) {
            ROS_ERROR("[SCENE-UTIL] Validation of object failed (a dimension is 0).");
            return false;
        }

        if (obj.id().empty()) {
            ROS_ERROR("[SCENE-UTIL] Validation of object failed (id not defined).");
            return false;
        }
    }

    return true;
}

bool SceneConstructorUtil::constructObjects(std::vector<SceneCollisionObject> &collision_objects,
                                            std::vector<std_msgs::ColorRGBA> &collision_objects_colors,
                                            const Scene &scene) {

    if (!validateObjects(scene)) {
        return false;
    }

    for (const auto &obj : scene.objects()) {
        if (obj.type() == Object::BOX) {

            moveit_msgs::CollisionObject c_obj;
            std_msgs::ColorRGBA obj_color;

            c_obj.header.frame_id = "panda_link0";
            c_obj.id = obj.id();

            c_obj.primitives.resize(1);
            c_obj.primitives[0].type = c_obj.primitives[0].BOX;
            c_obj.primitives[0].dimensions.resize(3);
            c_obj.primitives[0].dimensions[0] = obj.size().length();
            c_obj.primitives[0].dimensions[1] = obj.size().width();
            c_obj.primitives[0].dimensions[2] = obj.size().height();

            c_obj.primitive_poses.resize(1);
            c_obj.primitive_poses[0].position.x = obj.pos().x();
            c_obj.primitive_poses[0].position.y = obj.pos().y();
            c_obj.primitive_poses[0].position.z = obj.pos().z();

            c_obj.primitive_poses[0].orientation.w = obj.orientation().w();
            c_obj.primitive_poses[0].orientation.x = obj.orientation().x();
            c_obj.primitive_poses[0].orientation.y = obj.orientation().y();
            c_obj.primitive_poses[0].orientation.z = obj.orientation().z();

            c_obj.operation = c_obj.ADD;

            obj_color.r = obj.color().r();
            obj_color.g = obj.color().g();
            obj_color.b = obj.color().b();
            obj_color.a = 1;

            SceneCollisionObject o_sco;
            o_sco.setType(OBJECT_TYPE);
            o_sco.setCollisionObject(c_obj);
            collision_objects.push_back(o_sco);
            collision_objects_colors.push_back(obj_color);
        }
    }
    return true;
}