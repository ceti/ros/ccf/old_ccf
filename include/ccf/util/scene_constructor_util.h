//
// Created by Sebastian Ebert on 03.08.20.
//

#ifndef CCF_SCENE_CONSTRUCTOR_H
#define CCF_SCENE_CONSTRUCTOR_H


#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>

#include "connector.pb.h"
#include "scene_collision_object.h"

static const std::string TABLE_PILLAR_1 = "tablePillar1";
static const std::string TABLE_PILLAR_2 = "tablePillar2";
static const std::string TABLE_PILLAR_3 = "tablePillar3";
static const std::string TABLE_PILLAR_4 = "tablePillar4";
static const std::string TABLE_PLATE = "table";
static const std::string TABLE_PLATE_BUFFER = "table_buffer";

/**
* Util for the construction of the scene.
*/
class SceneConstructorUtil {

public:

    /**
     * Adds a table plate to the planning scene.
     * @param collision_objects current collision objects in the planning scene
     * @param x_dimension width of the table
     * @param y_dimension length of the table
     * @param with_buffer optionally adds a buffer space over the table
     */
    static bool constructTable(std::vector<SceneCollisionObject> &collision_objects,
                               std::vector<std_msgs::ColorRGBA> &collision_objects_colors, Scene &table);

    /**
     * Adds bins to the planning scene.
     * @param collision_objects current collision objects in the planning scene
     * @param collision_objects_colors current collision object colors in the planning scene
     * @param scene a scene object containing the bin objects
     */
    static bool constructBins(std::vector<SceneCollisionObject> &collision_objects,
                              std::vector<std_msgs::ColorRGBA> &collision_objects_colors, const Scene &scene);

    /**
     * Adds a list of objects to be picked/placed to the planning scene.
     * @param collision_objects current collision objects in the planning scene
     * @param collision_objects_colors current collision object colors in the planning scene
     * @param scene a scene object containing the objects
     */
    static bool constructObjects(std::vector<SceneCollisionObject> &collision_objects,
                                 std::vector<std_msgs::ColorRGBA> &collision_objects_colors, const Scene &scene);
};

#endif //CCF_SCENE_CONSTRUCTOR_H