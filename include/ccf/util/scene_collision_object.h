//
// Created by sebastian on 14.01.21.
//

#ifndef CCF_SCENECOLLISIONOBJECT_H
#define CCF_SCENECOLLISIONOBJECT_H

#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit/move_group_interface/move_group_interface.h>

static const std::string BIN_TYPE = "bin";
static const std::string PILLAR_TYPE = "pillar";
static const std::string PLATE_TYPE = "plate";
static const std::string BUFFER_TYPE = "buffer";
static const std::string OBJECT_TYPE = "object";

class SceneCollisionObject {

public:

    SceneCollisionObject();

    const moveit_msgs::CollisionObject &getCollisionObject() const;

    void setCollisionObject(const moveit_msgs::CollisionObject &collisionObject);

    const std::string &getType() const;

    void setType(const std::string &type);

private:

    moveit_msgs::CollisionObject collisionObject;

    std::string type;

};


#endif //CCF_SCENECOLLISIONOBJECT_H
