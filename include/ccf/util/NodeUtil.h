//
// Created by jm on 07/05/2021.
//

#ifndef CCF_NODEUTIL_H
#define CCF_NODEUTIL_H

#include <ros/ros.h>

#include <sstream>

namespace CetiRosToolbox {

    /// Gets a parameter from the parameter server. If the parameter does not exist, the fallback is used. If no fallback
    /// value is specified, an empty string is returned.
    ///
    /// If the parameter is \a not on the parameter server, a warning is issued at \c ROS_WARN.
    ///
    /// Usually, when using the method, the template parameter can be omitted if a fallback value is provided. This does
    /// \a not work when providing \c const \c char* as fallback (i.e., string constants). In this case, the template
    /// parameter must be provided:
    /// \code{.cpp}
    /// auto intValue1 = getParameter<int>(n, "foo"); // no fallback means no automatic matching
    /// auto intValue2 = getParameter(n, "foo", 42); // the compiler matches T with int, this is okay
    /// auto stringValue = getParameter<std::string>(n, "bar", "fourtytwo"); // const char* requires the std::string template parameter
    /// \endcode
    /// \tparam T a type supported by the parameter server.
    /// The parameter server supports \c std::string double \c float \c int \c bool
    /// and \c std::vector and \std::map of these types.
    /// Additionally, also \c XmlRpc::XmlRpcValue is supported, though not as a vector or map.
    /// \param n the NodeHandle, in the namespace of which the parameter is queried
    /// \param key the name of the parameter
    /// \param fallback the default value
    /// \return the parameter (if it exists), otherwise the fallback value (if it exists), otherwise the default
    ///         instance of the respective object
    /// \warning Currently, this is a header-only library, but this might change in the future
    template<class T>
    T getParameter(const ros::NodeHandle &n, const std::string &key, T fallback = T()) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default '" << fallback << "'.");
        }
        return fallback;
    }

    /// Gets a private parameter from the parameter server. If the parameter does not exist, the fallback is used.
    /// If no fallback value is specified, an empty string is returned.
    ///
    /// If the parameter is \a not on the parameter server, a warning is issued at \c ROS_WARN.
    ///
    /// Usually, when using the method, the template parameter can be omitted if a fallback value is provided. This does
    /// \a not work when providing \c const \c char* as fallback (i.e., string constants). In this case, the template
    /// parameter must be provided:
    /// \code{.cpp}
    /// auto intValue1 = getParameter<int>(n, "foo"); // no fallback means no automatic matching
    /// auto intValue2 = getParameter(n, "foo", 42); // the compiler matches T with int, this is okay
    /// auto stringValue = getParameter<std::string>(n, "bar", "fourtytwo"); // const char* requires the std::string template parameter
    /// \endcode
    /// \tparam T a type supported by the parameter server.
    /// The parameter server supports \c std::string double \c float \c int \c bool
    /// and \c std::vector and \std::map of these types.
    /// Additionally, also \c XmlRpc::XmlRpcValue is supported, though not as a vector or map.
    /// \param key the name of the parameter
    /// \param fallback the default value
    /// \return the parameter (if it exists), otherwise the fallback value (if it exists), otherwise the default
    ///         instance of the respective object
    /// \warning Currently, this is a header-only library, but this might change in the future
    template<class T>
    T getPrivateParameter(const std::string &key, T fallback = T()) {
        ros::NodeHandle n("~"); // a private node handle
        return getParameter(n, key, fallback);
    }

    /** @cond */ // the specializations are required to print the warning correctly
    template<>
    XmlRpc::XmlRpcValue getParameter(const ros::NodeHandle &n, const std::string &key, XmlRpc::XmlRpcValue fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default '" << fallback.toXml() << "'.");
        }
        return fallback;
    }
    template<>
    std::vector<std::string> getParameter(const ros::NodeHandle &n, const std::string &key, std::vector<std::string> fallback) {
        if (!n.getParam(key, fallback)) {
            std::ostringstream vector;
            std::copy(fallback.begin(), fallback.end(), std::ostream_iterator<std::string>(vector, " "));
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                                           << " from param server, using default [ " << vector.str() << "].");
        }
        return fallback;
    }
    template<>
    std::vector<double> getParameter(const ros::NodeHandle &n, const std::string &key, std::vector<double> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty vector.");
        }
        return fallback;
    }
    template<>
    std::vector<float> getParameter(const ros::NodeHandle &n, const std::string &key, std::vector<float> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty vector.");
        }
        return fallback;
    }
    template<>
    std::vector<int> getParameter(const ros::NodeHandle &n, const std::string &key, std::vector<int> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty vector.");
        }
        return fallback;
    }
    template<>
    std::vector<bool> getParameter(const ros::NodeHandle &n, const std::string &key, std::vector<bool> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty vector.");
        }
        return fallback;
    }
    template<>
    std::map<std::string, std::string> getParameter(const ros::NodeHandle &n, const std::string &key, std::map<std::string, std::string> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty map.");
        }
        return fallback;
    }
    template<>
    std::map<std::string, double> getParameter(const ros::NodeHandle &n, const std::string &key, std::map<std::string, double> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty map.");
        }
        return fallback;
    }
    template<>
    std::map<std::string, float> getParameter(const ros::NodeHandle &n, const std::string &key, std::map<std::string, float> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty map.");
        }
        return fallback;
    }
    template<>
    std::map<std::string, int> getParameter(const ros::NodeHandle &n, const std::string &key, std::map<std::string, int> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty map.");
        }
        return fallback;
    }
    template<>
    std::map<std::string, bool> getParameter(const ros::NodeHandle &n, const std::string &key, std::map<std::string, bool> fallback) {
        if (!n.getParam(key, fallback)) {
            ROS_WARN_STREAM("[" << ros::this_node::getName() << "] Could not get string value for "
                                << n.getNamespace() << "/" << key
                                << " from param server, using default empty map.");
        }
        return fallback;
    }
    /** @endcond */
}

#endif //CCF_NODEUTIL_H
