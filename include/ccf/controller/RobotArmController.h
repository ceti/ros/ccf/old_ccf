//
// Created by Johannes Mey on 17/01/2021.
//

#ifndef CCF_ROBOTARMCONTROLLER_H
#define CCF_ROBOTARMCONTROLLER_H

#include <functional>
#include <optional>

#include <ros/ros.h>

#include "ccf/connection/Connection.h"
#include "Controller.h"

#include <connector.pb.h>

class RobotArmController : public Controller {

private:

    std::function<void(Selection)> selectionAction;
    std::function<void()> sceneUpdateAction;
    std::function<void(PickPlace)> pickPlaceAction;

    void receive(const std::string &channel, const std::string &data) override;

    bool removeObject(const Object &object);

    std::string cellName;

    std::string selectionTopic;
    std::string initSceneTopic;
    std::string sendSceneTopic;
    std::string commandTopic;
public:
    const std::string &getSelectionTopic() const;

    void setSelectionTopic(const std::string &selectionTopic);

    const std::string &getInitSceneTopic() const;

    void setInitSceneTopic(const std::string &initSceneTopic);

    const std::string &getSendSceneTopic() const;

    void setSendSceneTopic(const std::string &sendSceneTopic);

    const std::string &getCommandTopic() const;

    void setCommandTopic(const std::string &commandTopic);

protected:
    ros::NodeHandle nodeHandle;
    std::vector<std::unique_ptr<Connection>> connections;
    std::shared_ptr<Scene> scene;
public:

    RobotArmController(const ros::NodeHandle &nodeHandle, const std::string &cellName);

    void loadScene(const std::string &sceneFile);

    std::shared_ptr<Scene> getScene();

    void sendScene();

    void initScene(const Scene &scene);

    // application "skills"
    virtual bool pickAndDrop(Object &robot, Object &object, Object &bin, bool simulateOnly);

    virtual bool pickAndPlace(Object &robot, Object &object, Object &location, bool simulateOnly);

    /// Compute if a location is reachable by a robot, i.e., if an object can be placed or picked at this location
    /// \param robot
    /// \param location
    /// \param object to be picked and placed. The current location of this object is ignored.
    /// \return true if the location is reachable
    virtual bool reachableLocation(const Object &robot, const Object &location, const Object &object) = 0;

    /// Compute if an object is reachable by a robot, i.e., if an object can grasp it
    /// \param robot
    /// \param object to be picked
    /// \return true if the location is reachable
    virtual bool reachableObject(const Object &robot, const Object &object) = 0;

    // reactive components: application logic provided by callbacks
    void reactToSelectionMessage(std::function<void(Selection)> lambda);

    void reactToPickAndPlaceMessage(std::function<void(PickPlace)> lambda);

    void reactToSceneUpdateMessage(std::function<void()> lambda);

    // helper methods
    Object *resolveObject(const std::string &id);
};

#endif //CCF_ROBOTARMCONTROLLER_H
