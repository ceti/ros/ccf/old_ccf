//
// Created by Johannes Mey on 17/01/2021.
//

#ifndef CCF_DUMMYROBOTARMCONTROLLER_H
#define CCF_DUMMYROBOTARMCONTROLLER_H

#include "RobotArmController.h"

class DummyRobotArmController : public RobotArmController {

public:
    explicit DummyRobotArmController(const ros::NodeHandle &nodeHandle, const std::string &cellName);

    bool pickAndDrop(Object &robot, Object &object, Object &bin, bool simulateOnly) override;

    bool pickAndPlace(Object &robot, Object &object, Object &location, bool simulateOnly) override;

    /// Compute if a location is reachable by a robot, i.e., if an object can be placed or picked at this location
    /// If the location is within a "cone" with a radius of 150mm around the robot base, it is too close to reach.
    //  If the location is more than 750mm away from the robot base it is too far away.
    //  Otherwise, we assume that we can reach it.
    /// \param robot
    /// \param location
    /// \param object to be picked and placed. The current location of this object is ignored.
    /// \return true if the location is reachable
    virtual bool reachableLocation(const Object &robot, const Object &location, const Object &object) override;

    /// Compute if an object is reachable by a robot, i.e., if an object can grasp it
    /// \param robot
    /// \param object to be picked
    /// \return true if the location is reachable
    virtual bool reachableObject(const Object &robot, const Object &object) override;
};

#endif //CCF_DUMMYROBOTARMCONTROLLER_H
