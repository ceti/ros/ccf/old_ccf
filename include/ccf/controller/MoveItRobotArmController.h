//
// Created by Johannes Mey on 17/01/2021.
//

#ifndef CCF_MOVEITROBOTARMCONTROLLER_H
#define CCF_MOVEITROBOTARMCONTROLLER_H

#include <ccf/SceneUpdate.h>

#include "RobotArmController.h"

class MoveItRobotArmController : public RobotArmController {

private:

    ros::ServiceClient pick_drop_client;
    ros::ServiceClient pick_place_client;
    ros::ServiceClient bin_check_client;
    ros::ServiceServer get_scene_service;

public:
    explicit MoveItRobotArmController(ros::NodeHandle &nodeHandle, const std::string &cellName);

    virtual ~MoveItRobotArmController();

    void updateScene(ccf::SceneUpdateRequest &req);

    bool pickAndDrop(Object &robot, Object &object, Object &bin, bool simulateOnly) override;

    bool pickAndPlace(Object &robot, Object &object, Object &location, bool simulateOnly) override;

    bool reachableLocation(const Object &robot, const Object &location, const Object &object) override;

    bool reachableObject(const Object &robot, const Object &object) override;
};


#endif //CCF_MOVEITROBOTARMCONTROLLER_H
