//
// Created by Johannes Mey on 17/01/2021.
//

#ifndef CCF_CONTROLLER_H
#define CCF_CONTROLLER_H

#include <functional>
#include <optional>

#include <ros/ros.h>

#include "ccf/connection/Connection.h"

#include <connector.pb.h>

class Controller {

private:


protected:
    ros::NodeHandle nodeHandle;
    std::vector<std::unique_ptr<Connection>> connections;

    virtual void receive(const std::string &channel, const std::string &data) = 0;

public:
    explicit Controller(const ros::NodeHandle &nodeHandle);

    // common functionality
    void addConnection(std::unique_ptr<Connection> &&connection);

    void sendToAll(const std::string &channel, const std::string &message);

};

#endif //CCF_CONTROLLER_H
