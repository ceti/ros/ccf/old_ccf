//
// Created by Sebastian Ebert on 04.05.21.
//

#ifndef CCF_MQTTCONNECTION_H
#define CCF_MQTTCONNECTION_H

#include "Connection.h"

#include <thread>
#include <mqtt/client.h>

class MqttConnection : public Connection {

    const std::string &mqtt_address;
    const std::string &client_id;
    std::unique_ptr<std::thread> mqtt_handler_thread;
    mqtt::client client;
    mqtt::string_collection topics;
    const int QOS = 0;

public:

    explicit MqttConnection(const std::string &mqttAddress, const std::string &client_id);

    bool send(const std::string &channel, const std::string &message) override;

    bool initializeConnection(std::function<void(std::string, std::string)> callback) override;

    void receiveMessages();

    void addTopic(const std::string &topic);

    ~MqttConnection();

private:

    mqtt::client &getClient();

    bool ensureConnection();
};

#endif //CCF_MQTTCONNECTION_H