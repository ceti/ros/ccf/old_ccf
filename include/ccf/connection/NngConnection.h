//
// Created by jm on 28/04/2021.
//

#ifndef CCF_NNGCONNECTION_H
#define CCF_NNGCONNECTION_H

#include "Connection.h"

#include <nng/nng.h>

#include <thread>

/// A connection over NNG using Pair in version 1
/// see https://nng.nanomsg.org/man/v0.2.0/nng_pair.html
class NngConnection : public Connection {

    const std::string &connection_address;
    const bool server;
    nng_socket sock{};
    std::unique_ptr<std::thread> nng_receiver_thread;
    std::string sendTopic;
    std::string receiveTopic;
public:

    /// Constructor requiring an address as defined in https://nng.nanomsg.org/man/v1.3.2/nng.7.html
    /// \param connection_address in server mode the port on which the connections listens for connections, in client
    ///        mode the address of the server
    /// \param server true, if the connection is a server (listens for connections), otherwise client mode is enabled
    explicit NngConnection(const std::string &connection_address, bool server = true);

    bool send(const std::string &channel, const std::string &message) override;

    /// NNG pair does not support topics, so when it receives a message, this topic is assumed
    [[nodiscard]] const std::string &getReceiveTopic() const;

    /// NNG pair does not support topics, so when it sends a message, this topic is the only one that is actually sent
    [[nodiscard]] const std::string &getSendTopic() const;

    /// set the topic that NNG actually sends, messages on other topics are ignored
    /// \param newTopic
    void setSendTopic(const std::string &newTopic);

    /// set the topic NNG forwards received messages with
    /// \param newTopic
    void setReceiveTopic(const std::string &newTopic);

    bool initializeConnection(std::function<void(std::string, std::string)> callback) override;

    void receiveMessages();

    ~NngConnection();
};


#endif //CCF_NNGCONNECTION_H
